# ethereum-bootnode

* Go Ethereum  
https://github.com/ethereum/go-ethereum

## Usage
### nodekey
```bash
docker run hawyasunaga/ethereum-bootnode /bin/sh -c 'bootnode --genkey=/tmp/nodekey && cat /tmp/nodekey'  
```

### address
```bash
docker run -e NODE_KEY=[YOUR_NODE_KEY] hawyasunaga/ethereum-bootnode /bin/sh -c 'bootnode --nodekeyhex=$NODE_KEY --writeaddress'   
```

### run
```bash
docker run -e NODE_KEY=[YOUR_NODE_KEY] hawyasunaga/ethereum-bootnode 
```
